//clone
const ptr = { x: 1, y: 2 };
const clone = obj1 => {
  const obj = {};
  for (let i in obj1) {
    obj[i] = obj1[i];
  }
  return obj;
};
console.log(clone({ x: 1, y: 2 }));

//assoc
const assoc = (k, v, obj) => {
  const arr = clone(obj);
  arr[k] = v;
  return arr;
};

console.log(assoc("c", 3, { x: 1, y: 2 }));

//dissoc
const dissoc = (obj, k) => {
  const res = {};
  for (const i in obj) {
    if (i != k) {
      res[i] = obj[i];
    }
  }
  return res;
};
console.log(dissoc({ x: 1, y: 2, c: 3 }, "y"));

//invert
const raceResults = {
  first: "alice",
  second: "jake"
};
const invert = obj => {
  const res = {};
  for (let i in obj) {
    res[obj[i]] = i;
  }
  return res;
};
console.log(invert(raceResults));


