//sorted-order

const isSorted = arr => {
  for (let e of arr) {
    if (arr[e] < arr[e + 1]) {
      return true;
    } else return false;
  }
};
console.log(isSorted([1, 6, 7, 8]));

//any-number-even--true;

const any = (pred, arr) => {
  let result = [];
  for (const e of arr) {
    result = pred(e);
  }
  return result;
};
const even = n => n % 2 === 0;

console.log(any(even, [3, 1, 7]));


