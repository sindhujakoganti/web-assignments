//factorial
const isFactorial = n => {
  let fact = 1;
  for (let i = 1; i <= n; i += 1) fact *= i;

  return fact;
};

//ncr
const ncr = (n, r) => isFactorial(n) / (isFactorial(r) * isFactorial(n - r));
//pascal-line
const pascal = n => {
  const num = [];
  for (let i = 0; i <= n; i += 1) num.push(ncr(n, i));
  return num;
};

//pascal-triangle
const ispascal = n => {
  const num = [];
  for (let j = 0; j <= n; j += 1) num[j] = pascal(j);
  return num;
};
console.log(ispascal(3));

