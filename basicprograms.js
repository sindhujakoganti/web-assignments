const isFactorial = n => {
  let fact = 1;
  for (let i = 1; i <= n; i += 1) fact *= i;

  return fact;
};

const ncr = (n, r) => isFactorial(n) / (isFactorial(r) * isFactorial(n - r));

const pascal = n => {
  const num = [];
  for (let i = 0; i <= n; i += 1) num.push(ncr(n, i));
  return num;
};

const ispascal = n => {
  const num = [];
  for (let j = 0; j <= n; j += 1) num[j] = pascal(j);
  return num;
};
console.log(ispascal(3));

// -------------------------square---------------------

const squareAll = arr => {
  const number = [];
  for (let i = 0; i < arr.length; i += 1) number.push(arr[i] * arr[i]);
  return number;
};
console.log(squareAll([2, 4, 6]));

// -----------------------even---------------------

const allEven = arr => {
  const number = [];
  for (let i = 0; i < arr.length; i += 1)
    if (arr[i] % 2 === 0) number.push(arr[i]);
  return number;
};
console.log(allEven([2, 3, 4, 24, 36, 66]));

<<<<<<< HEAD
// ----------------------sum----------------------------
const sumin = arr => {
  let sum = 0;
  for (let i = 0; i < arr.length; i += 1) sum += arr[i];
=======
//sum
function sumin(arr) {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum += arr[i];
  }
  return sum;
}
console.log(sumin([20, 30, 40]));
>>>>>>> b7cec6be6aab66675271f84a633663818462ffeb

  return sum;
};
console.log(sumin([4, 4, 6]));

// ................max...................

const maxOf = arr => {
  let max = 0;
  for (let i = 0; i < arr.length; i += 1) if (arr[i] > max) max = arr[i];

  return max;
};
console.log(maxOf([88, 70, 19]));

// --------------indexof------------------
function isindexOf(arr, value) {
  for (let i = 0; i < arr.length; i += 1) if (arr[i] === value) return i;
  return -1;
}
console.log(isindexOf([1, 2, 3], 3));

// repeat
function repeat(n, x) {
  const arr = [];
  for (let i = 1; i <= n; i += 1) arr.push(x);

  return arr;
}
console.log(repeat(5, '*'));

// ---------------power------------------

function power(x, y) {
  let mul = 1;
  for (let i = 1; i <= y; i += 1) mul *= x;

  return mul;
}
console.log(power(3, 3));

// take
const take = (num, arr) => {
  const result = [];
  for (let i = 0; i < num; i += 1) result.push(arr[i]);

  return result;
};
console.log(take(4, [1, 2, 3, 4, 5, 6, 7, 8]));


//-----------reverse-------------------

const reverse = arr => {
  const result = [];
  for (let i = 1; i <= arr.length; i++) {
    result.push(arr[arr.length - i]);
  }
  return result;
};
console.log(reverse([11, 12, 23, 43]));

//drop
const drop = (num, arr) => {
  const result = [];
  for (let i = num; i < arr.length; i++) {
    result.push(arr[i]);
  }
  return result;
};
console.log(drop(5, [34, 56, 38, 86, 45, 23, 78]));

//concat
const concat = (arr1, arr2) => {
  for (let i = 0; i < arr2.length; i++) {
    arr1.push(arr2[i]);
  }
  return arr1;
};
console.log(concat([1, 2, 3], [4, 5, 6]));

//prime
const isPrime = n => {
  if (n === 1) return true;
  for (let i = 2; i < n; i++)
    if (n % i === 0) return false;
    else return true;
};
console.log(isPrime(1));

//perfect
const isPerfect = n => {
  let sum = 0;
  for (let i = 0; i < n; i++) {
    if (n % i === 0) sum += i;
  }
  if (sum === n) return true;
  else false;
};
console.log(isPerfect(28));

//merge

const pt = { x: 1, y: 2 };
const size = { width: 10, height: 20 };

const merge = (obj1, obj2) => {
  let obj3 = {};
  for (let i in obj1) {
    obj3[i] = obj1[i];
  }
  for (let i in obj2) {
    obj3[i] = obj2[i];
  }
  return obj3;
};

console.log(merge(pt, size));

//pluck
const prop = { x: 1, y: 2, z: 3 };

const pluck = (ob, props) => {
  const result = {};
  for (const i of props) {
    result[i] = ob[i];
  }
  return result;
};
console.log(pluck(prop, ["x", "y"]));

//splitat
const splitAt = (arr, number) => {
  const obj = [];
  for (let i = 0; i < number; i++) {
    obj.push(arr[i]);
  }
  const obj2 = [];
  for (let i = number; i < arr.length; i++) {
    obj2.push(arr[i]);
  }
  return [obj, obj2];
};
console.log(splitAt([1, 2, 3, 4, 5], 2));

