const even = n => n % 2 === 0;

// map
const map = (pred, arr) => {
  const res = [];
  for (const e of arr) res[e] = pred(e);
  return res;
};
console.log(map(even, [1, 2, 3, 4, 5]));

// reduce
const add = (x, y) => x + y;
const reduce = (pred, init, arr) => {
  let res = init;
  for (let e = 0; e < arr.length; e += 1) res =pred(res,e);
  return res;
};
console.log(reduce(add, 0, [1, 2, 3, 4, 5, 6]));


//filter
function filter(pred, arr) {
  const result = [];
  for (const e of arr) if (pred(arr[e])) result.push(arr[e]);
  return result;
}
console.log(filter(even, [1, 2, 3, 4, 6]));

