const takeWhile = (pred, arr) => {
  const result = [];
  for (let e = 0; e < arr.length; e += 1)
    if (pred(arr[e])) result[e] = arr[e];
    else break;
  return result;
};
console.log(takeWhile(even, [2, 4, 6, 7, 6, 8]));
console.log(takeWhile(even, [1, 3, 6, 7, 6, 8]));

// reverse
const reverse = arr => {
  const result = [];
  for (let e = 0; e < arr.length; e += 1) result[e] = arr[arr.length - e - 1];
  return result;
};
console.log(reverse([11, 12, 23, 43]));

